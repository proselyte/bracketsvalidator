package net.proselyte.bracketsvalidator;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class BracketsValidatorTests {
    private BracketsValidator bracketsValidator = new BracketsValidator();

    private static final String correct = "[({})]";
    private static final String incorrect = "[([)";

    @Test
    public void shouldVerifyStringAsEmptyTest(){
        String result = bracketsValidator.verify("");
        assertEquals("String is empty", "String is correct", result);
    }

    @Test
    public void shouldVerifyStringAsCorrectTest(){
        String result = bracketsValidator.verify(correct);
        assertEquals("String is correct", "String is correct", result);
    }

    @Test
    public void shouldVerifyStringAsIncorrectTest(){
        String result = bracketsValidator.verify(incorrect);
        assertEquals("String is correct", "String is incorrect", result);
    }

    @Test
    public void shouldVerifyStringAsNestedTest(){
        String result = bracketsValidator.verify("bar{[(abc)]foo[def]}");
        assertEquals("String is correct", "String is correct", result);    }

    @Test
    public void shouldNotVerifyNullStringTest(){
        String result = bracketsValidator.verify(null);
        assertEquals("String is incorrect", "String is incorrect", result);    }
}
