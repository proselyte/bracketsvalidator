package net.proselyte.bracketsvalidator;

import java.util.*;

import static java.lang.System.*;

/**
 * Brackets validator designed to check is passed string has correct order of brackets.
 *
 * @author Eugene Suleimanov
 */

class BracketsValidator {
    private final Map<Character, Character> brackets = new HashMap<>(3);

    public BracketsValidator() {
        brackets.put('}', '{');
        brackets.put(']', '[');
        brackets.put(')', '(');
    }

    public String verify(String line) {

        if (line == null) {
            out.println("Input string cannot bu null");
            return "String is incorrect";
        }

        Set<Character> closedBrackets = brackets.keySet();
        Collection<Character> openBrackets = brackets.values();

        Deque<Character> charsStack = new ArrayDeque<>();
        for (char character : line.toCharArray()) {
            if (openBrackets.contains(character)) {
                charsStack.push(character);
            } else if (closedBrackets.contains(character)) {
                Character lastBracket = charsStack.peek();
                if (lastBracket == null) {
                    return "String is incorrect";
                }

                if (lastBracket.equals(brackets.get(character))) {
                    charsStack.pop();
                }
            }
        }

        return charsStack.isEmpty() ? "String is correct" : "String is incorrect";
    }
}
