package net.proselyte.bracketsvalidator;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import static java.lang.System.*;

public class BracketsValidatorDemo {
    public static BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(in));

    public static void main(String[] args) {
        out.println("Enter test data: ");

        String line = readString();

        BracketsValidator bracketsValidator = new BracketsValidator();
        out.println(bracketsValidator.verify(line));
    }

    private static String readString() {
        String line = "";
        try {
            line = bufferedReader.readLine();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return line;
    }
}
